import {useMutation, useQuery} from "react-query";
import {authLogin} from "./auth.query";
import {addUser, deleteUser, editUser, fetchUserDetail, fetchUsers} from "./users.query";

const endpoint = {
  login: "https://fakestoreapi.com/auth/login",
  users: "https://fakestoreapi.com/users",
}

export const useAuthLoginMutation = () => {
  return useMutation(
    ({username, password}) => authLogin({username, password, endpoint})
  )
}

export const useFetchUsers = (payload) => {
  return useQuery(
    [endpoint.users, JSON.stringify(payload)],
    async () => await fetchUsers({endpoint, payload}),
    {
      keepPreviousData: true
    }
  )
}

export const useFetchUserDetail = (payload) => {
  return useQuery(
    [endpoint.users, JSON.stringify(payload)],
    async () => await fetchUserDetail({endpoint, payload}),
    {
      keepPreviousData: true
    }
  )
}

export const useAddUser = () => {
  return useMutation(
    (payload) => addUser({endpoint, payload})
  )
}

export const useEditUser = () => {
  return useMutation(
    (id, payload) => editUser({id, endpoint, payload})
  )
}

export const useDeleteUser = () => {
  return useMutation(
    (id) => deleteUser({id, endpoint})
  )
}
