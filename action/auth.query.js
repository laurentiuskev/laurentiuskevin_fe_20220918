import axios from "axios";
import {setCookie} from "nookies";

export async function authLogin(payload) {
  const url = new URL(payload.endpoint.login)

  const {data, status} = await axios.post(url.toString(), {
    username: payload.username,
    password: payload.password
  })

  if (status >= 200 && status < 300) {
    setCookie(null, "token", data.token)
  }

  return data
}
