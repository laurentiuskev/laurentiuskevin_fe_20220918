import axios from "axios";

export async function fetchUsers({endpoint, payload}) {
  const url = new URL(endpoint.users);

  const {data} = await axios.get(url.toString(), {params: payload})
  return data
}

export async function fetchUserDetail({endpoint, payload}) {
  const url = new URL(endpoint.users);

  const {data} = await axios.get(url.toString() + `/${payload.id}`, {params: payload})
  return data
}

export async function addUser({endpoint, payload}) {
  const url = new URL(endpoint.users);

  const {data} = await axios.post(url.toString(), payload)
  return data
}

export async function editUser({id, endpoint, payload}) {
  const url = new URL(endpoint.users);

  const {data} = await axios.put(url.toString()+`/${id}`, payload)
  return data
}

export async function deleteUser({id, endpoint}) {
  const url = new URL(endpoint.users);

  const {data} = await axios.delete(url.toString()+`/${id}`)
  return data
}
