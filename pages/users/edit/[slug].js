import React, {useEffect, useState} from "react";
import nookies from "nookies";
import {useRouter} from "next/router";
import {useForm} from "react-hook-form";
import {useEditUser, useFetchUserDetail} from "../../../action/mutations.query";
import UserEditForm from "../../../components/forms/user-edit.form";
import Head from "next/head";

export async function getServerSideProps(context) {
  const cookies = nookies.get(context)

  if (cookies.token === undefined) {
    return {
      redirect: {
        destination: "/sign-in"
      }
    }
  }

  return {
    props: {
      token: cookies.token
    }
  }
}

export default function EditUser(props) {
  const router = useRouter()
  const {register, handleSubmit, formState: {errors}} = useForm()

  const {
    slug
  } = router.query

  const defaultPayload = {
    id: slug
  }

  const [payload, setPayload] = useState(defaultPayload)

  const {
    isLoading: fetchIsLoading,
    isError: fetchIsError,
    error: fetchError,
    isSuccess: fetchIsSuccess,
    data: fetchData
  } = useFetchUserDetail({
    token: props.token,
    ...payload
  })

  const {
    mutate: editUser,
    isLoading,
    isError,
    error,
    isSuccess
  } = useEditUser()

  const onSubmit = (data) => {
    editUser(fetchData.id,data)
  }

  useEffect(() => {
    if (isSuccess) {
      router.push("/")
    }
  }, [isSuccess])

  return (
    <div>
      <Head>
        <title>Edit User</title>
        <meta name="description" content="Generated by create next app"/>
        <link rel="icon" href="/favicon.ico"/>
      </Head>

      <div className="h-full w-full py-16 px-4">
        <div className="flex flex-col items-center justify-center">
          <div className="bg-gray-100 shadow rounded lg:w-1/3  md:w-1/2 w-full p-10 mt-16">
            <p aria-label="Login to your account" className="mb-5 text-2xl font-extrabold leading-6 text-gray-800">
              Edit User
            </p>

            <UserEditForm
              onSubmit={onSubmit}
              isLoading={isLoading}
              isError={isError}
              error={error}
              data={fetchData}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
