import React from "react";

export default function Pagination(props) {
  return (
    <div className="flex items-start mt-12 sm:mt-0 sm:ml-4">
      <div className="border flex bg-white text-gray-500 shadow rounded">
        <button
          className="py-3 px-5 flex items-center justify-center text-xs focus:outline-none"
          onClick={() => {
            props.setPayload(prevState => ({
              ...prevState,
              limit: (prevState.limit > 5 ? prevState.limit-5 : 5)
            }))
          }}
        >Previous</button>
        <button
          className="py-3 px-5 flex items-center justify-center text-xs focus:outline-none border border-l border-r-0 border-b-0 border-t-0 border-indigo-100"
        >Limit: {props.payload.limit}</button>
        <button
          className="py-3 px-5 flex items-center justify-center text-xs focus:outline-none border border-l border-r-0 border-b-0 border-t-0 border-indigo-100"
          onClick={() => {
            props.setPayload(prevState => ({
              ...prevState,
              limit: prevState.limit + 5
            }))
          }}
        >Next</button>
      </div>
    </div>
  )
}
