import React from "react";
import {useForm} from "react-hook-form";

export default function UserAddForm({onSubmit, isLoading, isError, error}) {
  const {
    register,
    handleSubmit,
    formState: {errors}
  } = useForm()

  return (
    <form className="mt-8 space-y-6" onSubmit={handleSubmit(onSubmit)}>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">Email</label>
        <input
          {...register("email", {required: "Email wajib diisi!"})}
          type="text"
          className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2"
        />
        {
          errors.email?.type === "required"
          && <div className="text-red-500">{errors.email?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">Username</label>
        <input
          {...register("username", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.username?.type === "required"
          && <div className="text-red-500">{errors.username?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">Password</label>
        <input
          {...register("password", {required: "Username wajib diisi!"})}
          type="password" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.password?.type === "required"
          && <div className="text-red-500">{errors.password?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">First Name</label>
        <input
          {...register("name.firstname", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.name?.firstname?.type === "required"
          && <div className="text-red-500">{errors.name?.firstname?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">Last Name</label>
        <input
          {...register("name.lastname", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.name?.lastname?.type === "required"
          && <div className="text-red-500">{errors.name?.lastname?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">No. Telp</label>
        <input
          {...register("phone", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.phone?.type === "required"
          && <div className="text-red-500">{errors.phone?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">Kota</label>
        <input
          {...register("address.city", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.address?.city?.type === "required"
          && <div className="text-red-500">{errors.address?.city?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">Alamat</label>
        <input
          {...register("address.street", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.address?.street?.type === "required"
          && <div className="text-red-500">{errors.address?.street?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">No. Rumah</label>
        <input
          {...register("address.number", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.address?.number?.type === "required"
          && <div className="text-red-500">{errors.address?.number?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">Zip Code</label>
        <input
          {...register("address.zipcode", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.address?.zipcode?.type === "required"
          && <div className="text-red-500">{errors.address?.zipcode?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">Latitude</label>
        <input
          {...register("address.geolocation.lat", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.address?.geolocation?.lat?.type === "required"
          && <div className="text-red-500">{errors.address?.geolocation?.lat?.message}</div>
        }
      </div>
      <div>
        <label className="text-sm font-medium leading-none text-gray-800">Longitude</label>
        <input
          {...register("address.geolocation.long", {required: "Username wajib diisi!"})}
          type="text" className="border rounded focus:outline-none text-xs font-medium leading-none text-gray-800 py-3 w-full pl-3 mt-2" />
        {
          errors.address?.geolocation?.long?.type === "required"
          && <div className="text-red-500">{errors.address?.geolocation?.long?.message}</div>
        }
      </div>
      <div className="mt-8">
        <button type="submit" aria-label="create my account" className="focus:ring-2 focus:ring-offset-2 focus:ring-indigo-700 text-sm font-semibold leading-none text-white focus:outline-none bg-indigo-700 border rounded hover:bg-indigo-600 py-4 w-full">
          Simpan
        </button>
      </div>
    </form>
  )
}
