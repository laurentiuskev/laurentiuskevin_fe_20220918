import React from "react";
import {useForm} from "react-hook-form";

export default function LoginForm({onSubmit, isLoading, isError, error}) {
  const {register, handleSubmit, formState: {errors}} = useForm()

  return (
    <form className="mt-8 space-y-6" onSubmit={handleSubmit(onSubmit)}>
      <div className="flex flex-col">
        <label
          htmlFor="username"
          className="text-black text-sm font-bold leading-tight tracking-normal mb-2"
        >
          Username
        </label>
        <input
          {...register("username", {required: "Username wajib diisi!"})}
          id="username"
          className="focus:outline-none focus:border focus:border-indigo-700 dark:focus:border-indigo-700 dark:border-gray-700 bg-white font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border shadow"
          placeholder="Input Username"
          defaultValue="mor_2314"
        />
        {
          errors.username?.type === "required"
          && <div className="text-red-500">{errors.username?.message}</div>
        }
      </div>
      <div className="flex flex-col">
        <label
          htmlFor="password"
          className="text-black text-sm font-bold leading-tight tracking-normal mb-2"
        >
          Password
        </label>
        <input
          {...register("password", {required: "Password wajib diisi!"})}
          id="password"
          type="password"
          className="focus:outline-none focus:border focus:border-indigo-700 dark:focus:border-indigo-700 dark:border-gray-700 bg-white font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border shadow"
          placeholder="Input Password"
          defaultValue="83r5^_"
        />
        {
          errors.password?.type === "required"
          && <div className="text-red-500">{errors.password?.message}</div>
        }
      </div>

      <div>
        {isError && <span className="text-red-500">{error?.response?.data}</span>}
        <button type="submit"
          className="group relative flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
          <span className="absolute inset-y-0 left-0 flex items-center pl-3">
            <svg className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
              <path fillRule="evenodd"
                d="M10 1a4.5 4.5 0 00-4.5 4.5V9H5a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-6a2 2 0 00-2-2h-.5V5.5A4.5 4.5 0 0010 1zm3 8V5.5a3 3 0 10-6 0V9h6z"
                clipRule="evenodd"/>
            </svg>
          </span>
          {
            isLoading === false
              ? "Sign In"
              : "Loading"
          }
        </button>
      </div>
    </form>
  )
}
