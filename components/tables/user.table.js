import React from "react";
import Link from "next/link";

export default function UserTable(props) {
  return (
    <table className="w-full whitespace-nowrap">
      <thead>
        <tr className="h-16 w-full text-sm leading-none text-gray-800">
          <th className="font-normal text-left">ID</th>
          <th className="font-normal text-left">Name</th>
          <th className="font-normal text-left">Email</th>
          <th className="font-normal text-left">Phone</th>
          <th className="font-normal text-left">Action</th>
        </tr>
      </thead>
      <tbody className="w-full">
        {props.data?.map((item, key) => (
          <tr key={key} className="h-20 text-sm leading-none text-gray-800 bg-white hover:bg-gray-100 border-b border-t border-gray-100">
            <td className="">
              <p className="text-sm font-medium leading-none text-gray-800">{item.id}</p>
            </td>
            <td className="">
              <p className="text-sm font-medium leading-none text-gray-800">{item.name.firstname + " " + item.name.lastname}</p>
            </td>
            <td className="">
              <p className="text-sm font-medium leading-none text-gray-800">{item.email}</p>
            </td>
            <td className="">
              <p className="text-sm font-medium leading-none text-gray-800">{item.phone}</p>
            </td>
            <td className="">
              <Link
                href={`/${item.id}`}
              >
                <button
                  className="mx-2 my-2 bg-indigo-400 transition duration-150 ease-in-out focus:outline-none rounded text-white border border-gray-300 px-6 py-2 text-xs"
                >Lihat</button>
              </Link>
              <Link
                href={`/users/edit/${item.id}`}
              >
                <button
                  className="mx-2 my-2 bg-indigo-400 transition duration-150 ease-in-out focus:outline-none rounded text-white border border-gray-300 px-6 py-2 text-xs"
                >Edit</button>
              </Link>
              <button
                onClick={() => {
                  props.handleDelete(item.id)
                }}
                className="mx-2 my-2 bg-indigo-400 transition duration-150 ease-in-out focus:outline-none rounded text-white border border-gray-300 px-6 py-2 text-xs"
              >Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}
